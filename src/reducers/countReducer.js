const initState = {
  value: 0,
  isLoading: false,
};

function reducer(state = initState, action) {
  switch (action.type) {
    case 'SUCCES_ADD': {
      return {
        ...state,
        value: action.payload.number,
        isLoading: false,
      };
    }
    case 'START_ADD': {
      return {
        ...state,
        isLoading: true,
      };
    }
    default:
      return state;
  }
}

export default reducer;
