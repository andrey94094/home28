import { put, takeEvery} from 'redux-saga/effects';

const delay = (ms) => new Promise((res)=> setTimeout(res,ms))

export function* incrementAsync() {
  yield put({ type: 'START_ADD'});
  yield delay(2000);
  const number = Math.floor(Math.random() * 10);
  yield put({ type: 'SUCCES_ADD', payload:{ number } });
}

export default function* countSaga() {
  yield takeEvery('FETCH_ADD', incrementAsync);
}
