import React, { Component } from 'react';
import { connect } from 'react-redux';
import { addCount } from '../../actionCreators';
import './Button.scss';

class Bt extends Component {
  render() {
    const { isLoading, add } = this.props;
    return (
      <button disabled={isLoading} className='bt' onClick={() => add()}>
        {this.props.value}
      </button>
    );
  }
}

const mapStateToProps = ({ count }) => ({
  value: count.value,
  isLoading: count.isLoading,
});

const mapDispatchToProps = (dispatch) => ({
  add: (reddit) => dispatch(addCount(reddit)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Bt);
