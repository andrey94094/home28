import { Fragment } from 'react';
import Button from '../Button/Button';
import './App.scss';

function App() {
  return (
    <Fragment>
      <main>
        <Button />
      </main>
    </Fragment>
  );
}

export default App;
