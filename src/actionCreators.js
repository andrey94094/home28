export function addCount(reddit) {
  return {
    type: 'FETCH_ADD',
    payload: {
      reddit,
    },
  };
}
